import java.util.*;
 class Statistics 
{
	Scanner scan = new Scanner (System.in);
	public void getInput()
	{
		HashMap<String,Integer>listOfSports = new HashMap<>();
		HashMap<String,String>listOfEntries = new HashMap<>();
		int noOfEntries=scan.nextInt();
		for (int entries=0; entries < noOfEntries; entries++)
		{
			String name = scan.next();
			String sport = scan.next();

			if(listOfEntries.containsKey(name))
			  continue;
			else
			{
				if(!listOfSports.containsKey(sport)) 
				{
					listOfSports.put(sport,1);
				}
				else 
				{
					listOfSports.put(sport,listOfSports.get(sport)+1);
				}
			}
			listOfEntries.put(name,sport);
		}
		compareFans(listOfSports);
	}

	public void compareFans(HashMap<String,Integer> listOfSports)
	{
		String resultingFavSport="";
		int maxFans=0;
		Set<String> sportNames = listOfSports.keySet();
		for (String sportname : sportNames) 
		{
			int likers = listOfSports.get(sportname);
			if (likers > maxFans) 
			{
				maxFans = likers;
				resultingFavSport = sportname;
			}
		}
		printResult(resultingFavSport,listOfSports.get("football"));			

	}
	public void printResult(String resultingFavSport,Integer footballFans  )
	{
		System.out.println(resultingFavSport);
		System.out.println(footballFans==null?"0":footballFans);
	}
	public static void main(String args[] ) {
		Statistics statistics = new Statistics();  
		statistics.getInput();

	}             
}