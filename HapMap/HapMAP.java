/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package javaapplication1;
import java.util.*;
/**
*
* @author laxman.mali
*/
public class HapMAP<K, V>
{
	LinkList<K, V> node[] = new LinkList[16]; 
	public  K key;
	public  V value;
	public int hashCode(K key) 
	{
		int result =  key.hashCode()>0?key.hashCode():(0-key.hashCode());
		return result;
	}

	public  void put(K key, V value)
	{
		boolean flag = false;
		LinkList<K,V> linkList = new LinkList();
		LinkList<K,V> helperLinkList = new LinkList();
		LinkList<K,V> priviousLinkList = new LinkList();

		int hash =  hashCode(key);
		int hashvalue = hash%16;
		linkList.setAllValue(key, value,null);
		if(node[hashvalue]!=null)
		{


			helperLinkList =  node[hashvalue];
			priviousLinkList = helperLinkList;
			while(helperLinkList!=null)
			{

				if(helperLinkList.getKey().equals(key))
				{

					helperLinkList.SetValue(value);
					flag = true;
					break;
				}
					priviousLinkList  = helperLinkList;
					helperLinkList = helperLinkList.getNode(); 

			}
			if(!flag)
			{


				priviousLinkList.setNode(linkList);

			}


		}
		else
		{
			node[hashvalue]= linkList; 
		}

	}
	public V get(K key) 
	{
		V value = null;
		LinkList<K,V> linkList = new LinkList();
		int hash =  hashCode(key);
		int hashvalue = hash%16;
		linkList = node[hashvalue];

		if(linkList!=null)
		{
			while(linkList!=null)
			{
				if(linkList.getKey().equals(key))
				{
				value = linkList.getValue();

				}
				linkList = linkList.getNode();

			}


		}

		return value;


	}

	public boolean containsKey(K key)
	{
		boolean flag = false;
		LinkList<K,V> linkList = new LinkList();
		int hash =  hashCode(key);
		int hashvalue = hash%16;
		linkList = node[hashvalue];

		while(linkList!=null)
		{
			 if(linkList.getKey().equals(key))
			{

				flag = true; 
				break;
			}
			linkList = linkList.getNode();   
		}



		return flag;

	}

	public List<K> keySet()
	{
		List<K> keyData = new ArrayList<K>();
		for(int count = 0;count<node.length; count++)
		{


			LinkList<K,V> linkList = new LinkList();
			linkList =  node[count];
			while(linkList!=null)
			{

				keyData.add(linkList.getKey());
				linkList = linkList.getNode(); 
			}


		}

		return keyData; 
	}
        
        
      public void remove(K key)
      {
      
      
    
		LinkList<K,V> linkList = new LinkList();
        LinkList<K,V> previousNode = new LinkList();
		int hash =  hashCode(key);
		int hashvalue = hash%16;
		linkList = node[hashvalue];

		if(linkList!=null)
		{
          if(!linkList.getKey().equals(key))
		  {
              previousNode = linkList;
              while(linkList!=null)
			  {
				if(linkList.getKey().equals(key))
				{
				
                                 previousNode.setNode(linkList.getNode());
                                 break;
				}
                previousNode = linkList;
				linkList = linkList.getNode();

			   }
                    }
                    else{
                
                    node[hashvalue]=linkList.getNode();
                    }
		


		}

      
      }


}