/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author laxman.mali
 */

public class LinkList <K,V> 
{
	K key;
	V value;
	LinkList<K,V> next;  
	public void setAllValue(K key, V value,LinkList next )  
	{
		this.key = key;  
		this.value = value;   
		this.next = next; 
	}
	public void setNode(LinkList next)
	{
		this.next = next;
	}
	public LinkList getNode()
	{
		return this.next;
	}
	public K getKey()
	{
		return this.key;
	}
	public V getValue()
	{
		return this.value;
	}
        public void SetValue(V value)
	{
	 this.value = value;
	}
}