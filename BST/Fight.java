/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binearysearchtree;

import java.util.Scanner;

/**
 *
 * @author laxman.mali
 */
public class Fight 
{
	Scanner scan = new Scanner (System.in);
	int height=0;

	public void getInput()
	{
		int noOfEntries=scan.nextInt();
		BinearySearchTree rootNode = new BinearySearchTree();
		rootNode = null;

		for (int entries=0; entries < noOfEntries; entries++)
		{
			int  data = scan.nextInt();
			rootNode = insert(rootNode,data);
		}

		getHeight(rootNode,0); 
		System.out.println(height);


	}


	BinearySearchTree  newNode(int data) 
	{
		BinearySearchTree childNode = new BinearySearchTree();
        childNode.setData(data);
		childNode.setLeftNode(null);
		childNode.setRightNode(null);
		return childNode;
	}

	public BinearySearchTree insert(BinearySearchTree Node, int data)
	{
	if (Node == null)
	{
		return newNode(data);
    } 
	
	else
	{

		if (data <= Node.getData())
		{
		 Node.setLeftNode(insert(Node.getLeftNode(), data));	
		}
			
		else
		{
		 Node.setRightNode(insert(Node.getRightNode(), data));
		}  

		return Node; 
	}

	}  

	public void printTree(BinearySearchTree Node)
	{
		if(Node!=null)
		{
			System.out.println("NODE"+Node.getData());
			Nodecount(Node.getLeftNode());
			Nodecount(Node.getRightNode());
		}
	}

	public void getHeight(BinearySearchTree Node,int count)
	{

		if(Node!=null)
		{
			count++;
			if(height<count)
			{
				height = count;
			}

			getHeight(Node.getLeftNode(),count);
			getHeight(Node.getRightNode(),count);
		}


	}

	public static void main(String args[] ) 
	{

		Fight fight = new Fight(); 	
		fight.getInput();
	}   

}
