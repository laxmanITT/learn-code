
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.tests.AllTestsTest.JUnit4Test;

public class FightTest extends JUnit4Test
{
	@Test
	public void testHeightOfWhenNull()
	{
		Fight binarySearchTree = new Fight();
		BinearySearchTree rootNode = null;
		assertEquals(0, binarySearchTree.getHeight(rootNode));
	}
	@Test
	public void testTreeHeightWithLeftChlid()
	{
		Fight binarySearchTree = new Fight();
		BinearySearchTree rootNode = null;
		rootNode = binarySearchTree.insert(rootNode, 5);
		rootNode = binarySearchTree.insert(rootNode, 4);
		rootNode = binarySearchTree.insert(rootNode, 3);
		rootNode = binarySearchTree.insert(rootNode, 2);
		rootNode = binarySearchTree.insert(rootNode, 1);
		assertEquals(5, binarySearchTree.getHeight(rootNode));
	}
	@Test
	public void testTreeHeightWithRightChild()
	{
		Fight binarySearchTree = new Fight();
		BinearySearchTree rootNode = null;
		rootNode = binarySearchTree.insert(rootNode, 1);
		rootNode = binarySearchTree.insert(rootNode, 2);
		rootNode = binarySearchTree.insert(rootNode, 3);
		rootNode = binarySearchTree.insert(rootNode, 4);
		rootNode = binarySearchTree.insert(rootNode, 5);
		assertEquals(5, binarySearchTree.getHeight(rootNode));
	}
	@Test
	public void testTreeHeightWithLeftRightChild()
	{
		Fight binarySearchTree = new Fight();
		BinearySearchTree rootNode = null;
		rootNode = binarySearchTree.insert(rootNode, 8);
		rootNode = binarySearchTree.insert(rootNode, 3);
		rootNode = binarySearchTree.insert(rootNode, 6);
		rootNode = binarySearchTree.insert(rootNode, 5);
		rootNode = binarySearchTree.insert(rootNode, 7);
		rootNode = binarySearchTree.insert(rootNode, 4);
		assertEquals(5, binarySearchTree.getHeight(rootNode));
	}
	@Test
	public void testTreeHeightWithRightLeftChild()
	{
		Fight binarySearchTree = new Fight();
		BinearySearchTree rootNode = null;
		rootNode = binarySearchTree.insert(rootNode, 2);
		rootNode = binarySearchTree.insert(rootNode, 8);
		rootNode = binarySearchTree.insert(rootNode, 3);
		rootNode = binarySearchTree.insert(rootNode, 12);
		rootNode = binarySearchTree.insert(rootNode, 7);
		rootNode = binarySearchTree.insert(rootNode, 6);
		rootNode = binarySearchTree.insert(rootNode, 5);
		rootNode = binarySearchTree.insert(rootNode, 4);
		assertEquals(7, binarySearchTree.getHeight(rootNode));
	}
	@Test
	public void testNewNodeCheckNotNull()
	{
		Fight binarySearchTree = new Fight();
		assertNotNull(binarySearchTree.newNode(5));
	}
	@Test
	public void testNewNodeCheckNodeValue()
	{
		Fight binarySearchTree = new Fight();
		assertEquals(5,(binarySearchTree.newNode(5)).getData());
		assertNull((binarySearchTree.newNode(5)).getLeftNode());
		assertNull((binarySearchTree.newNode(5)).getRightNode());
	}
	@Test
	public void testInsertNodeWhenTreeEmpty()
	{
		BinearySearchTree BinearySearchTree = null;
		Fight binarySearchTree = new Fight();
		assertEquals(1, binarySearchTree.getHeight(binarySearchTree.insert(BinearySearchTree, 5)));
	}
}
