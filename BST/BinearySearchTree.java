/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package binearysearchtree;

/**
*
* @author laxman.mali
*/
public class BinearySearchTree 
{

	BinearySearchTree leftNode;
	int data;
	BinearySearchTree rightNode;
	public BinearySearchTree getLeftNode() 
	{
	 return leftNode;
	}

	public void setLeftNode(BinearySearchTree leftNode) 
	{
	 this.leftNode = leftNode;
	}

	public int getData()
	{
	 return data;
	}

	public void setData(int data) 
	{
	 this.data = data;
	}

	public BinearySearchTree getRightNode() 
	{
	 return rightNode;
	}

	public void setRightNode(BinearySearchTree rightNode) 
	{
	  this.rightNode = rightNode;
	}




}
