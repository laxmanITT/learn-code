import {DependencyContainer} from "../../dbLayer/repositories/providerConfiguration/dependencyContainer";
import {ZoneTag} from "../../server/zoneTag/zoneTag";
import * as repoType from "../../dbLayer/enums/repositoryType";
import RepositoryType = repoType.cartos.dbLayer.RepositoryType;

let async = require("async"),
    includes = require("array-includes"),
    objectId = require("mongodb").ObjectID,
    accessoryClass = require("../../server/accessory/accessory").Accessory,
    accessory: any,
    asset = require("../../server/asset/asset.js"),
    asm = require("../../server/assetSelection/assetSelectionService").assetSelectionService,
    env = process.env.NODE_ENV = process.env.NODE_ENV || "development",
    config = require("../../server/config/config")[env],
    errorFactory = require("../../server/errorHandler/errorFactory.js"),
    level = require("../../server/level/level.js"),
    log = require("../../server/config/logger.js").LOG,
    outcome = require("../../server/outcome/outcome.js"),
    pageVolumeTransfer = require("../../server/pageVolume/pageVolume.js"),
    pricingInfo = require("../../server/pricingInfo/pricingInfo.js"),
    projectSetting = require("../../server/settings/projectSetting.js"),

    zoneTag: ZoneTag,
    assetAccessoryMapping: any = {},
    mappings: any;

export class LevelHelper {
  fsChunks: RepositoryType.FsChunks;
  fsFile: RepositoryType.FsFile;
  levelDataUOW: RepositoryType.LevelData;

  /**
   * getLevels. import the level from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param collections - content of empty data and we will store data in collection */
  getLevels(db: any, projectId: string, collections: any) {
    return function (callback: any) {
      level.getLevels(db, projectId, true, function (err: any, levels: any) {
        if (err) {
          log.error("exportProject : error occurred while fetching levels for: " + projectId);
        } else if (!levels || levels.length === 0) {
          log.debug("exportProject : no record for levels to duplicate for:" + projectId);
          callback(new errorFactory.NoRecord(), null);
        } else {
          collections.level = levels;
        }
        callback(err, null);
      });
    };
  }

  /**
   * getProjectSetting. get projectSettings of that level from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param collections - content of empty data and we will store data in collection */
  getProjectSetting(db: any, projectId: string, collections: any) {
    return function (callback: any) {
      this.levelDataUOW = DependencyContainer.Instance.resolve(RepositoryType.LevelData);
      this.levelDataUOW.getProjectSettings(projectId, db, function (err: any, settings: any) {
        if (!settings || settings.length === 0) {
          log.debug("getProjectSettings- No :" + projectId);
          callback();
        } else if (settings) {
          let projectSettings = {
            _id: settings[0]._id,
            projectId: projectId,
            levelSettings: settings[0].levelSettings,
            pricingSettings: settings[0].pricingSettings,
            lastUpdatedMapId: settings[0].lastUpdatedMapId || 0
          };
          collections.projectSettings = [projectSettings];
          callback();
        } else {
          log.error("getProjectSettings- Error finding project details in database: ", projectId);
          callback(err, null);
        }
      });
    };
  }


  /**
   * getAssets. import the Assets from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param collections - content of empty data and we will store data in collection */
  getAssets(db: any, projectId: string, collections: any) {
    return function (callback: any) {
      asset.getAssetsByLevelId(db, projectId, true, function (err: any, assets: any) {
        if (!assets || assets.length === 0) {
          log.debug("exportProject : no assets found for:" + projectId);
          callback();
        } else if (assets) {
          collections.asset = assets;
          callback();
        } else {
          log.debug("exportProject : error occurred while fetching assets for:" + projectId);
          callback(err, null);
        }
      });
    };
  }

  /**
   * getAccessories. import the Accessories from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param collections - content of empty data and we will store data in collection */
  getAccessories(db: any, projectId: string, collections: any) {
    accessory = new accessoryClass();
    return function (callback: any) {
      accessory.getAssetAccessoryMap(db, {projectId: projectId}, function (err: any, assetAccessoryMap: any) {
        if (err) {
          log.debug("exportProject : error occurred while fetching accessories for :", projectId);
          callback();
        } else if (!assetAccessoryMap || !assetAccessoryMap.length) {
          log.debug("exportProject : no accessories found for:" + projectId);
          callback();
        } else {
          collections.assetAccessoryMap = assetAccessoryMap;
          callback();
        }
      });
    };
  }

  /**
   * getVirtualAssets. import the virtual asset from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param collections - content of empty data and we will store data in collection */
  getVirtualAssets(db: any, projectId: string, collections: any) {
    return function (callback: any) {
      asset.getVirtualAssets(db, projectId, config.transitionName.name, function (err: any, virtualAssets: any) {
        if (!virtualAssets || virtualAssets.length === 0) {
          log.debug("exportProject : no virtual assets found for:" + projectId);
          callback();
        } else if (virtualAssets) {
          collections.virtualAssets = virtualAssets;
          callback();
        } else {
          log.debug("exportProject : error occurred while fetching virtual assets for:" + projectId);
          callback(err, null);
        }
      });
    };
  }

  /**
   * getFsFiles. import the FsFiles from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param mediaIds : contains the  media from level entety map.
   * @param collections - content of empty data and we will store data in collection */
  getFsFiles(db: any, projectId: string, mediaIds: any, collections: any) {
    return function (callback: any) {
      this.fsFile = DependencyContainer.Instance.resolve(RepositoryType.FsFile);
      this.fsFile.getFsFiles(mediaIds, db, function (err: any, fsFiles: any) {
        if (err) {
          log.error("exportProject - Database Error : ", err);
          callback(err, null);
        } else if (!fsFiles || fsFiles.length === 0) {
          log.debug("exportProject : no fsFiles found for:" + projectId);
          callback();
        } else {
          collections["fs.files"] = fsFiles;
          callback();
        }
      }.bind(this));
    }.bind(this);
  }


  /**
   * getFsChunks. import the FsChunks from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param mediaIds : contains the  media from level entety map.
   * @param collections - content of empty data and we will store data in collection */
  getFsChunks(db: any, projectId: string, mediaIds: any, collections: any) {
    return function (callback: any) {
      this.fsChunks = DependencyContainer.Instance.resolve(RepositoryType.FsChunks);
      this.fsChunks.getFsChunks(mediaIds, db, function (err: any, fsChunks: any) {
        if (err) {
          log.error("exportProject - Database Error : ", err);
          callback(err, null);
        } else if (!fsChunks || fsChunks.length === 0) {
          log.debug("exportProject : no fsChunks found for:" + projectId);
          callback();
        } else {
          collections["fs.chunks"] = fsChunks;
          callback();
        }
      }.bind(this));
    }.bind(this);
  }

  /**
   * getPageVolumeTransfer. import the PageVolumeTransfer from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param collections - content of empty data and we will store data in collection */
  getPageVolumeTransfer(db: any, projectId: string, collections: any) {
    return function (callback: any) {
      pageVolumeTransfer.getPageVolumeTransfer(db, projectId, null, function (err: any, pageVolumeTransfers: any) {
        if (!pageVolumeTransfers || pageVolumeTransfers.length === 0) {
          log.debug("export collections : no pageVolumeTransfer is found for :" + projectId);
          callback();
        } else if (pageVolumeTransfers) {
          collections.pageVolumeTransfer = pageVolumeTransfers;
          callback();
        } else {
          log.debug("export collections: error occurred while fetching pageVolumeTransfer for:" + projectId);
          callback(err, null);
        }
      });
    };
  }

  /**
   * getOutcome. import the Outcome from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param collections - content of empty data and we will store data in collection */
  getOutcome(db: any, projectId: string, collections: any) {
    return function (callback: any) {
      outcome.getOutcome(db, projectId, null, function (err: any, outcomes: any) {
        if (!outcomes || outcomes.length === 0) {
          log.debug("export collections: no Outcome to duplicate for:" + projectId);
          callback();
        } else if (outcomes) {
          collections.outcome = outcomes;
          callback();
        } else {
          log.debug("export collections: error occurred while fetching outcome for:" + projectId);
          callback(err, null);
        }
      });
    };
  }

  /**
   * getPricingInfo. import the Pricing from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param collections - content of empty data and we will store data in collection */
  getPricingInfo(db: any, projectId: string, collections: any) {
    return function (callback: any) {
      let result = {projectId: projectId};
      pricingInfo.getAssetPricing(db, result, function (err: any, pricingInfo: any) {
        if (!pricingInfo || pricingInfo.length === 0) {
          log.debug("export collections: no Pricing to duplicate for:" + projectId);
          callback();
        } else if (pricingInfo) {
          collections.pricing = pricingInfo;
          callback();
        } else {
          log.debug("export collections: error occurred while fetching pricing for:" + projectId);
          callback(err, null);
        }
      });
    };
  }

  /**
   * getPricingInfo. import the Pricing from database and store in collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param collections - content of empty data and we will store data in collection */
  getZoneTagInfo(db: any, projectId: string, collections: any) {
    zoneTag = new ZoneTag();
    return function (callback: any) {
      zoneTag.getZoneTags(db, projectId, function (err: any, tag: any) {
        if (!tag || tag.length === 0) {
          log.debug("export collections: no zoneTag to duplicate for:" + projectId);
          callback();
        } else if (tag) {
          collections.tag = tag;
          callback();
        } else {
          log.debug("export collections: error occurred while fetching zoneTag for:" + projectId);
          callback(err, null);
        }
      });
    };
  }

  /**
   * setCollection. import all collection from databse and store into collection
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   * @param mediaIds : contains the  media from level entety map.
   * @param collections - content of empty data and we will store data in collection .
   * @param next - Callback.
   */
  public setCollection(db: any, projectId: string, mediaIds: Array<string>, collections: any, next: any) {
    async.parallel({
          getLevels: this.getLevels(db, projectId, collections),
          getAssets: this.getAssets(db, projectId, collections),
          getAccessories: this.getAccessories(db, projectId, collections),
          virtualAssets: this.getVirtualAssets(db, projectId, collections),
          getFsFiles: this.getFsFiles(db, projectId, mediaIds, collections),
          getFsChunks: this.getFsChunks(db, projectId, mediaIds, collections),
          pageVolumeTransfer: this.getPageVolumeTransfer(db, projectId, collections),
          getOutcome: this.getOutcome(db, projectId, collections),
          getPricingInfo: this.getPricingInfo(db, projectId, collections),
          getZoneTagInfo: this.getZoneTagInfo(db, projectId, collections),
          getProjectSetting: this.getProjectSetting(db, projectId, collections)
        },
        function (err: any, results: any) {
          if (err) {
            next(err, null);
          } else {
            next(null, collections);
          }
        }.bind(this));
  }

  /**
   * generating new ids and mapping with old ids. updating all the _id fields of collections
   * @param collections - contains all the data for the project.
   * @param next - Callback.
   */
  public generateAndAssignNewIds(collections: any, next: any) {
    let mappedIds: any[] = [];
    mappings = {
      assets: {},
      levels: {},
      project: {}
    };
    for (let key in collections) {
      if (collections[key].length) {
        collections[key].forEach(function (data: any) {
          let oldId = data._id;
          let newId = data._id = new objectId();
          //1st phase refactor
          if (key === config.databaseCollectionNames.asset || key === config.databaseCollectionNames.level ||
              key === config.databaseCollectionNames["fs.files"] || key === config.databaseCollectionNames.tag) {
            //isAssetCopied true if asset is plotted // billing purpose
            if (key === config.databaseCollectionNames.asset) {
              mappings.assets[oldId.toString()] = newId.toString();
              assetAccessoryMapping[oldId.toString()] = newId.toString();
              if (data.plotted) {
                data.isAssetCopied = true;
              }
            }
            if (key === config.databaseCollectionNames.level) {
              mappings.levels[oldId.toString()] = newId.toString();
              LevelHelper.updateDefaultPvtSetting(data);
            }
            mappedIds.push({key: oldId.toString(), value: newId.toString()});
          }
        }.bind(this));
      }
    }

    next({
      mappedIds: mappedIds,
      mappings: mappings
    });
  }

  /**
   * updating collections; by replacing old id with new id.
   * @param collections - contains all the data for the project.
   * @param mappedIds - mappedIds.
   */

  public updateCollections(collections: any, mappedIds: any) {
    mappedIds.forEach(function (mappedId: any) {
      collections.level.forEach(function (level: any) {
        //updating parentId with new ids
        if (level.parentId && level.parentId === mappedId.key) {
          level.parentId = mappedId.value;
        }
        if (level.zoneTagId && level.zoneTagId === mappedId.key) {
          level.zoneTagId = mappedId.value;
        }
        //updating ancestors with the new ids
        if (level.ancestors) {
          for (let count = 0; count < level.ancestors.length; count++) {
            if (level.ancestors[count] === mappedId.key) {
              level.ancestors[count] = mappedId.value;
            }
          }
        }
        let currentDateTime = new Date().getTime();
        //set level creation and last updated date-time to current date-time
        level.createdDate = currentDateTime;
        level.lastModifiedDate = currentDateTime;

      });


      for (let index = 0; index < collections.levelEntityMap.length; index++) {
        //updating levelEntityMap with the new ids
        if (collections.levelEntityMap[index].levelId && collections.levelEntityMap[index].levelId === mappedId.key) {
          collections.levelEntityMap[index].levelId = mappedId.value;
        }
        if (collections.levelEntityMap[index].entityId && collections.levelEntityMap[index].entityId === mappedId.key) {
          collections.levelEntityMap[index].entityId = mappedId.value;
        }

        if (collections.levelEntityMap[index].zoneIds) {
          for (let count = 0; count < collections.levelEntityMap[index].zoneIds.length; count++) {
            if (collections.levelEntityMap[index].zoneIds[count] === mappedId.key) {
              collections.levelEntityMap[index].zoneIds[count] = mappedId.value;
            }
          }
        }
      }


      collections.outcome.forEach(function (outcome: any) {
        //updating outcome with the new ids

        if (outcome.levelId && outcome.levelId === mappedId.key) {
          outcome.levelId = mappedId.value;
        }

        if (outcome.assetId && outcome.assetId === mappedId.key) {
          outcome.assetId = mappedId.value;
        }
        if (outcome.originalAssetId && outcome.originalAssetId === mappedId.key) {
          outcome.originalAssetId = mappedId.value;
        }

        if (outcome.originalLevelId && outcome.originalLevelId === mappedId.key) {
          outcome.originalLevelId = mappedId.value;
        }
        if (outcome.replacedByAssetId && outcome.replacedByAssetId === mappedId.key) {
          outcome.replacedByAssetId = mappedId.value;
        }
        if (outcome.zoneIds) {
          for (let count = 0; count < outcome.zoneIds.length; count++) {
            if (outcome.zoneIds[count] === mappedId.key) {
              outcome.zoneIds[count] = mappedId.value;
            }
          }
        }
      });

      collections.pricing.forEach(function (pricing: any) {
        //updating pricing with the new ids

        if (pricing.projectId && pricing.projectId === mappedId.key) {
          pricing.projectId = mappedId.value;
        }

        if (pricing.assetId && pricing.assetId === mappedId.key) {
          pricing.assetId = mappedId.value;
        }
      });
      collections.tag.forEach(function (tag: any) {
        //updating pricing with the new ids
        if (tag.projectId && tag.projectId === mappedId.key) {
          tag.projectId = mappedId.value;
        }
      });
      collections.projectSettings.forEach(function (projectSetting: any) {
        //updating projectId with the mappedId
        if (projectSetting.projectId && projectSetting.projectId === mappedId.key) {
          projectSetting.projectId = mappedId.value;
        }
        if (projectSetting.levelSettings && projectSetting.levelSettings.retainLevelId === mappedId.key) {
          projectSetting.levelSettings.retainLevelId = mappedId.value;
        }
      });
      collections.pageVolumeTransfer.forEach(function (pageVolume: any) {
        //updating pageVolume with the new ids

        if (pageVolume.fromAssetId && pageVolume.fromAssetId === mappedId.key) {
          pageVolume.fromAssetId = mappedId.value;
        }

        if (pageVolume.toAssetId && pageVolume.toAssetId === mappedId.key) {
          pageVolume.toAssetId = mappedId.value;
        }
      });
      collections["fs.chunks"].forEach(function (data: any) {
        //updating faChunks with the new ids
        if (data.files_id && data.files_id.toString() === mappedId.key) {
          data.files_id = objectId(mappedId.value);
        }
      });

    });

  }

  /**
   * function to update the accessoriesmap.
   * @param assetAccessoryMap
   * @param projectId
   */
  public updateAccessoryMap(assetAccessoryMap: any, projectId: string) {
    assetAccessoryMap.forEach(function (accessoryMap: any) {
      accessoryMap.assetId = assetAccessoryMapping[accessoryMap.assetId];
      accessoryMap.projectId = projectId;
    });
  }

  /**
   * Function to update default PVT setting for copy project
   * @param level : contains the level.
   */

  static updateDefaultPvtSetting(level: any) {
    if (level.type === config.levelType.floor) {
      level.levelSpecificData.pvtSetting.flagPageVolumeArrowsEnabled = true;
      level.levelSpecificData.pvtSetting.flagPageVolumeLabelEnabled = false;
      level.levelSpecificData.pvtSetting.flagPageVolumeRingEnabled = true;
    } else if (level.type === config.levelType.building) {
      level.levelSpecificData.applyAllFloorWithInBuilding.pvtSetting = false;
    }
  }

  /**
   * Update copy Project Name
   * @param collections : contains the  collections.
   *  @param oldProject : contains the  old Project Data.
   */
  public static updateProjectName(collections: any, oldProject: any) {
    for (let index = 0; index < collections.level.length; index++) {
      //checking if the project have copyCount field
      if (collections.level[index].type === config.levelType.project) {
        if (!collections.level[index].copyCount) {
          /* //renaming the name of the copied project
           // for eg: first copy the file name should be <old project name>- copy.
           // For subsequent copies, the number of copy should be there. Much like copy file behavior in windows.
           // ex:<old project name>-copy(1) so on and so forth.*/
          collections.level[index].name = collections.level[index].name + "-copy";
        } else {
          collections.level[index].name = collections.level[index].name + "-copy" + "(" + (oldProject.copyCount + 1) + ")";
        }
        //should be deleted  not to be updated in fresh project
        delete collections.level[index].copyCount;
        break;
      }
    }

  }
}
