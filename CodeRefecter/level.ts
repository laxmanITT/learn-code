import {DependencyContainer} from "../../dbLayer/repositories/providerConfiguration/dependencyContainer";
import * as repoType from "../../dbLayer/enums/repositoryType";
import RepositoryType = repoType.cartos.dbLayer.RepositoryType;
import {Error} from "../../server/errorHandler/models/error";
import {LevelHelper} from "./levelHelper";
import {PricingService} from "../../worker/pricingInfo/pricingService";
import * as folderRequest from "../../server/folder/model/folderRequest";
import * as Enums from "../../server/folder/enum/folder";
import UpdateType = Enums.folder.enums.UpdateType;
import Prefix = Enums.folder.enums.Prefix;
import UpdateRequest = folderRequest.folder.models.FolderUpdateRequest;


let _ = require("lodash"),
    async = require("async"),
    includes = require("array-includes"),
    objectId = require("mongodb").ObjectID,
    asm = require("../../server/assetSelection/assetSelectionService").assetSelectionService,
    authorize = require("../../server/serviceRef/uaam/authorize"),
    authorizeConfig = require("../../server/serviceRef/serviceConfig").uaam.authorizeAttributes,
    env = process.env.NODE_ENV = process.env.NODE_ENV || "development",
    config = require("../../server/config/config")[env],
    errorFactory = require("../../server/errorHandler/errorFactory.js"),
    level = require("../../server/level/level.js"),
    levelEntityMap = require("../../server/levelEntityMap/levelEntityMap.js"),
    log = require("../../server/config/logger.js").LOG,
    projectSetting = require("../../server/settings/projectSetting.js"),
    asmService = new asm(objectId, errorFactory, log, includes);

export class Level {
  connection: any;
  levelDataUOW: RepositoryType.LevelData;
  levelHelper: LevelHelper;
  private pricingService: PricingService;
  folderRepository: RepositoryType.Folder;

  constructor() {
    this.levelHelper = new LevelHelper();
    this.pricingService = new PricingService();
  }

  /**
   * This function get and insert the old project settings.
   * @param db : contains the database object.
   * @param oldProjectId : contains the old project id.
   * @param newProjectId : contains the new project id.
   *  @param next : callback.
   */
  copyProjectSettings(db: any, oldProjectId: string, newProjectId: string, next: any) {
    async.waterfall([
      function (cb: any) {
        //get Project Setting form database
        projectSetting.getProjectSettings(db, {projectId: oldProjectId}, cb);
      },
      function (setting: any, cb: any) {
        let request = {
          projectId: newProjectId,
          lastUpdatedMapId: setting.lastUpdatedMapId || 0
        };
        projectSetting.insertProjectSetting(db, request, cb);
      },
    ], function (err: any, data: any) {
      if (err) {
        log.error("copyProjectSettings error occured:", err);
        next(err, null);
      } else {
        next(null, newProjectId);
      }
    });
  }


  /**
   * function to get all the getLevelEntityMap By ActiveAssets
   * @param db - Contains db objects.
   * @param collections - contains all objects where the data has to be put.
   * @param projectId - contains the project id.
   * @param next - Callback.
   */
  getDataByProjectId(db: any, collections: any, projectId: string, next: any) {
    let mediaIds: Array<string> = [];
    levelEntityMap.getLevelEntityMapByLevelId(db, projectId, true, config.entityType.media, function (err: any, levelEntityMapResult: any) {
      if (levelEntityMapResult) {
        for (let index = 0; index < levelEntityMapResult.length; index++) {
          mediaIds.push(objectId(levelEntityMapResult[index].entityId));
        }
      }
      this.levelHelper.setCollection(db, projectId, mediaIds, collections, next);
    }.bind(this));
  }

  /**
   * getting all the collections for copyProject.
   * @param db : contains the database object.
   * @param projectId : contains the  project id.
   *  @param next : callback.
   */

  public exportProject(db: any, projectId: any, next: any) {
    //TODO : dont change the order required for billing and metring, will be take care after
    let collections: {
        level: any[],
        "fs.files": any[],
        "fs.chunks": any[],
        asset: any[],
        outcome: any[],
        levelEntityMap: any[],
        pageVolumeTransfer: any[],
        virtualAssets: any[],
        assetAccessoryMap: any[],
        pricing: any[],
        tag: any[]
        projectSettings: any[]
    } = {
        level: [],
        "fs.files": [],
        "fs.chunks": [],
        asset: [],
        outcome: [],
        levelEntityMap: [],
        pageVolumeTransfer: [],
        virtualAssets: [],
        assetAccessoryMap: [],
        pricing: [],
        tag: [],
        projectSettings: []
    };

    async.waterfall([
      function (cb: any) {
        //get all the data apart form levelEntityMap
        this.getDataByProjectId(db, collections, projectId, cb);
      }.bind(this),
      function (collections: any, cb: any) {
        //get all the levelEntityMap
        this.getLevelEntityMapByActiveAssets(db, collections, next);
      }.bind(this),
    ], function (err: any, collections: any) {
      if (err) {
        log.error("exportProject -  error occoured while fetching data", err);
        next(err, null);
      } else {
        next(null, collections);
      }
    });
  }

  /**
   * function to get all the getLevelEntityMap By ActiveAssets
   * @param db - Contains db objects.
   * @param collections - contains all the data for the project.
   * @param next - Callback.
   */
  getLevelEntityMapByActiveAssets(db: any, collections: any, next: any) {
    let activeAssetsId = collections.asset.map(function (asset: any) {
      return asset._id.toString();
    });
    let levelIds = collections.level.map(function (level: any) {
      return level._id.toString();
    });

    levelEntityMap.filterLevelEntityMapByActiveAssets(db, levelIds, activeAssetsId, function (err: any, levelEntityMaps: any) {

      if (levelEntityMaps) {
        collections.levelEntityMap = levelEntityMaps;
      } else {
        log.error("getLevelEntityMapByActiveAssets : error occurred while fetching level Entity maps");
        return next(err, null);
      }
      //join both virtual assets and real assets
      collections.asset = collections.asset.concat(collections.virtualAssets);
      delete collections.virtualAssets;

      next(null, collections);
    });

  }


  /**
   * inserting all the updated collections
   * @param db : contains the database object.
   * @param sessionDetail : contains the  sessionDetail.
   * @param collections : contains the  collections.
   *  @param userId : contains the  userId.
   *  @param next : callback.
   */

  public importProject(sessionDetail: any, db: any, collections: any, userId: string, parentId: object, next: any) {
    let oldProject: any;
    oldProject = collections.level.find(function (level: any) {
      return (level.type === config.levelType.project);
    });
    oldProject.parentId = parentId;
    oldProject = _.cloneDeep(oldProject);
    LevelHelper.updateProjectName(collections, oldProject);
    this.levelHelper.generateAndAssignNewIds(collections, (result: any) => {
      let newProjectId = collections.level.find(function (level: any) {
        return (level.type === config.levelType.project);
      })._id.toString();
      result.mappings.project[oldProject._id.toString()] = newProjectId;
      //update the collections
      this.levelHelper.updateCollections(collections, result.mappedIds);
      this.levelHelper.updateAccessoryMap(collections.assetAccessoryMap, newProjectId);
      log.info("copyProject- Updating collection is done");
      async.parallel([
        (callback: Function) => {
          this.updateCollection(sessionDetail, db, collections, oldProject, userId, callback);
        },
        (callback: Function) => {
          this.pricingService.copyProject(oldProject._id.toString(), result.mappings, callback);
        }
      ], (err: Error, result: any[]) => {
        if (err) {
          if (result.length) {
            next(err, newProjectId);
          } else {
            next(err, null);
          }
        } else {
          next(null, newProjectId);
        }
      });
    });
  }

  /**
   * inserting all the updated collections to the database
   * @param db : contains the database object.
   * @param sessionDetail : contains the  sessionDetail.
   * @param collections : contains the  collections.
   *  @param oldProject : contains the  old Project Data.
   *  @param userId : contains the  userId.
   *  @param next : callback.
   */
  public updateCollection(sessionDetail: any, db: any, collections: any, oldProject: any, userId: string, next: any) {
    let newProject: any;
    this.levelDataUOW = DependencyContainer.Instance.resolve(RepositoryType.LevelData);
    this.folderRepository = DependencyContainer.Instance.resolve(RepositoryType.Folder);
    async.forEachSeries(Object.keys(collections), function (key: any, callback: any) {
      if (collections[key].length) {
        if (key === config.gridFs["fs.chunks"]) {
          //TODO: need to use gridStore while saving
          async.forEachSeries(collections[key], function (data: any, cb: any) {
            this.levelDataUOW.insertLevelData("fs.chunks", data, db, function (err: any, data: any) {
              if (err) {
                log.error("importProject  - DB Error : ", err);
                cb(err, null);
              } else {
                cb();
              }
            }.bind(this));
          }.bind(this), function (err: any, data: any) {
            if (err) {
              callback(err, null);
            } else {
              log.debug("chunks updated");
              callback(null, data);
            }
          }.bind(this));
        } else {
          async.waterfall([
            (cb: Function) => {
              this.levelDataUOW.insertLevelData(key, collections[key], db, (err: any, data: any) => {
                if (err) {
                  log.error("importProject  - DB Error : ", err);
                  cb(err, null);
                } else {
                  log.debug("updated ", key);
                  //TODO: "need to get rid of hard coding"
                  if (key === config.databaseCollectionNames.level) {
                    newProject = data.find((newLevel: any) => {
                      return (newLevel.type === config.levelType.project);
                    });
                  }
                  cb();
                }
              });
            },
            (cb: Function) => {
              if (newProject && newProject.parentId) {
                let updateKey: any = {};
                updateKey[Prefix.PROJECT + newProject._id] = newProject.name;
                let updateRequest = new UpdateRequest({
                  type: UpdateType.$set,
                  updates: updateKey
                });
                log.debug("updating the parent folder");
                this.folderRepository.updateFolderById(db, objectId(newProject.parentId), updateRequest, cb);
              } else {
                cb();
              }
            }
          ], (err: Error, res: any) => {
            if (err) {
              log.debug("error inserting level folder folder", err);
              callback(err, null);
            } else {
              log.debug("success everything okay");
              callback();
            }
          });
        }
      } else {
        callback();
      }
    }.bind(this), function (err: any, data: any) {
      if (err) {
        next(err, null);
      } else {
        let newProjectId = newProject._id.toString(),
            oldProjectId = oldProject._id.toString();

        let accessData = {
          levelId: newProjectId,
          userId: userId,
          permissions: authorizeConfig.permissions.own// provide own permission to owner
        };

        //After the project is copied successfully, increment the copyCount field of original project by 1.
        //copyCount property inside project denotes the number of times the project has been copied
        let request = [{id: oldProjectId, copyCount: (oldProject.copyCount || 0) + 1}];
        level.updateLevel(db, request, function (err: any, data: any) {
          if (data) {
            log.debug("old project is updated");

            //add permission for copy project
            authorize.addProjectLevelAccess(sessionDetail, accessData, function (err: any, response: any) {
              if (err) {
                //To avoid inconsistency, delete newly created project if there is any error updating the existing project.
                level.removeLevel(db, newProjectId, next);
              } else {
                asmService.copyProjectAsm(db, oldProjectId, newProjectId, function (err: any, res: any) {
                  next(null, newProjectId);
                }.bind(this));
              }
            }.bind(this));

          } else {
            //To avoid inconsistency, delete newly created project if there is any error updating the existing project.
            level.removeLevel(db, newProjectId, next);
          }
        }.bind(this));
      }
    }.bind(this));
  }


  /**
   * In copyProject we exports project data and import that
   * @param sessionDetail : contains the  sessionDetail.
   * @param reqData : reqData coming from front side.
   *  @param next : callback.
   */
  public copyProject(sessionDetail: any, reqData: any, next: any) {
    this.connection = DependencyContainer.Instance.getConnectionManager();
    this.connection.getConnection({dbName: sessionDetail.headers.session.dbName}, function (err: any, db: any) {
      if (err) {
        log.error("error while fetching database instance: ", err);
        next(err, null);
      } else {
        //getting all the data related to a project
        this.exportProject(db, reqData.projectId, function (err: any, collections: any) {
          if (err) {
            next(new errorFactory.OtherError({message: "Error exporting project data"}), null);
          } else {
            log.info("Copy project - Export project is completed succesfully");
            //updated collections with new ids and inserting back to database
            this.importProject(sessionDetail, db, collections, reqData.userId, reqData.parentId, function (err: any, data: any) {
              next(err, {data: data});
            }.bind(this));
          }
        }.bind(this));
      }
    }.bind(this));
  }

  /**
   * This is a wrapper function around addLevel function. This function will accept tenantDetails and creates the db object.
   * @param requestHeaders - user request headers
   * @param levelInfo - level data which needs to be added as a level
   * @param next
   */
  public addLevel(requestHeaders: any, levelInfo: any, next: any) {
    this.connection = DependencyContainer.Instance.getConnectionManager();
    this.connection.getConnection({dbName: requestHeaders.headers.session.dbName}, function (err: any, db: any) {
      if (err) {
        log.error("error while fetching database instance: ", err);
        next(err, null);
      } else {
        level.addLevel(requestHeaders, db, levelInfo, requestHeaders.headers.session.userId, function (err: any, data: any) {
          next(err, data);
        });
      }
    });
  }

  /**
   * This is a wrapper function around removeLevel function. This function will accept tenantDetails and creates the db object.
   * @param requestHeaders - user request headers
   * @param levelId - level data which needs to be deleted
   * @param next
   */
  public removeLevel(requestHeaders: any, levelId: string, next: any) {
    this.connection = DependencyContainer.Instance.getConnectionManager();
    this.connection.getConnection({dbName: requestHeaders.headers.session.dbName}, function (err: any, db: any) {
      if (err) {
        log.error("error while fetching database instance: ", err);
        next(err, null);
      } else {
        level.removeLevel(db, levelId, function (err: any, data: any) {
          next(err, data);
        });
      }
    });
  }
}
