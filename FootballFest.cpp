#include <iostream>
using namespace std;
bool isValidInput(int upperLimit,int value)
{
    return (0<value<=upperLimit) ; 
}


int main() 
{

    int testCase;
    int noOfPasses,currentPlayerId, lastPlayerId;  
     cin>>testCase;//get no of total case

    if(isValidInput(100,testCase)){
        while(testCase--) 
        {

            cin>>noOfPasses;//get no of passes
            cin>>currentPlayerId; //get current playerId
            if(isValidInput(100000,noOfPasses)||isValidInput(1000000,currentPlayerId))
            {

                while(noOfPasses--)
                {

                    char playerAction;
                    cin>>playerAction; //get player action Pass or Back 

                    if(playerAction == 'P')
                    {

                        lastPlayerId = currentPlayerId;
                        cin>>currentPlayerId;// get again player Id
                        if(!isValidInput(1000000,currentPlayerId))
                        {
                         
                           cin>>currentPlayerId;  // if id is invalid get again player Id
                        }

                    }
                    else if(playerAction == 'B')
                    {
                        currentPlayerId= lastPlayerId+ currentPlayerId;
                        lastPlayerId=currentPlayerId-lastPlayerId;
                        currentPlayerId=currentPlayerId-lastPlayerId;
                    }
                }
            }

            cout<<"Player "<<currentPlayerId<<endl;
        }
    }

    return 0;
}





