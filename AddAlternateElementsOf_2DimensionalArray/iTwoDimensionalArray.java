
public interface iTwoDimensionalArray {
	public int addElementsOfTwoDimensionalArrayWhenSumOfIndexIsEven(int[][] Array);

	public int addElementsOfTwoDimensionalArrayWhenSumOfIndexIsOdd(int[][] Array);

	public void input();

	public void printData(int[] Array);

	public int[] processOfData(int[][] Array);

}
