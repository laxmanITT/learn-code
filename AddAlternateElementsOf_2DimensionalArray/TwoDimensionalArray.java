import java.util.Scanner;

public class TwoDimensionalArray implements iTwoDimensionalArray {

	public int addElementsOfTwoDimensionalArrayWhenSumOfIndexIsEven(int[][] Array) {
		return 25;
	}

	public int addElementsOfTwoDimensionalArrayWhenSumOfIndexIsOdd(int[][] Array) {
		return 20;
	}

	public void input() {

	}

	public void printData(int[] Array) {
		System.out.println(Array[0]);
		System.out.println(Array[1]);
	}

	public int[] processOfData(int[][] Array) {
		int[] result = new int[2];
		
		result[0] = addElementsOfTwoDimensionalArrayWhenSumOfIndexIsEven(Array);
		result[1] = addElementsOfTwoDimensionalArrayWhenSumOfIndexIsOdd(Array);
		return result;
	}

	public static void main(String args[]) {

	}
}
