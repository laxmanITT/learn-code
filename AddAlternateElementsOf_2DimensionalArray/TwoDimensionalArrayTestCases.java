import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.tests.AllTestsTest.JUnit4Test;
import org.junit.tests.AllTestsTest.JUnit4Test;

public class TwoDimensionalArrayTestCases extends JUnit4Test {

	@Test
	public void testaddElementsOfTwoDimensionalArrayWhenSumOfIndexIsEven() {

		int[][] testData = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		TwoDimensionalArray nuberSum = new TwoDimensionalArray();
		assertEquals(25, (nuberSum.addElementsOfTwoDimensionalArrayWhenSumOfIndexIsEven(testData)));

	}

	@Test
	public void testaddElementsOfTwoDimensionalArrayWhenSumOfIndexIsOdd() {
		int[][] testData = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		TwoDimensionalArray nuberSum = new TwoDimensionalArray();
		assertEquals(20, (nuberSum.addElementsOfTwoDimensionalArrayWhenSumOfIndexIsOdd(testData)));

	}

	@Test
	public void testProcessOfData() {
		TwoDimensionalArray nuberSum = new TwoDimensionalArray();
		int[] result = new int[2];
		int[][] testData = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		result[0] = nuberSum.addElementsOfTwoDimensionalArrayWhenSumOfIndexIsEven(testData);
		result[1] = nuberSum.addElementsOfTwoDimensionalArrayWhenSumOfIndexIsOdd(testData);

		assertEquals(25, (nuberSum.processOfData(testData)[0]));
		assertEquals(20, (nuberSum.processOfData(testData)[1]));

	}
}