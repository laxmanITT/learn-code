import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

class Spider
{
 private int index;
 private int power;
 Spider(int index, int power)
 {
  this.index = index;
  this.power = power;
 }
 public int getIndex()
 {
  return index;
 }
 public int getPower()
 {
  return power;
 }
 public void setPower(int power)
 {
  this.power = power;
 }
}

public class Chamber
{

  private  Scanner input;
  private  int queueSize;
  private  int selectSpiders;
  private  Queue <Spider> spiderList;

  private  boolean isValidInput(int queueSize, int selectSpiders)
   {
    return (queueSize >= selectSpiders && queueSize <= (selectSpiders * selectSpiders) && selectSpiders > 0 && selectSpiders < 317);
   } 
  private  boolean isValidPower(int spiderPower, int selectSpiders)
   {
     return (spiderPower >= 1 && spiderPower <= (selectSpiders * selectSpiders));
   }
 
public void getInput()
  {
      input = new Scanner(System.in);
      queueSize = input.nextInt();
      selectSpiders = input.nextInt();
      spiderList = new LinkedList <Spider>();
      int temporaryValue;
      if (isValidInput(queueSize, selectSpiders))
         {
            for (int index = 1; index <= queueSize; index++)
            {
                temporaryValue = input.nextInt();
                if (isValidPower(temporaryValue, selectSpiders))
                {
                          spiderList.add(new Spider(index, temporaryValue));
                }
            }
       ChamberOfSecrets(selectSpiders, spiderList);
        }
   }

private  int spidersToBeSelected(Queue <Spider> spiderList, int selectSpiders)
  {
       int selectedSpiders;
       if (spiderList.size() < selectSpiders)
       {
          selectedSpiders = spiderList.size();   
       }
       else
       {
          selectedSpiders = selectSpiders;   
       }
      return selectedSpiders;
  }

 private  int  findMaxPowerIndex(ArrayList <Spider> spiders)
 {
    int maxPower = 0;
    int maxIndex = 0;
    for (int index = 0; index < spiders.size(); index++)
        {
          if (maxPower < spiders.get(index).getPower())
          {
            maxPower = spiders.get(index).getPower();
            maxIndex = index;
          }
        }
        return maxIndex;
 }

 private  ArrayList <Spider> decreaseSpidersPower(ArrayList <Spider> spiders)
 {
   for (Spider sp: spiders)
   {
    if (sp.getPower() != 0)
     sp.setPower(sp.getPower() - 1);
   }
   return spiders;
 }

 private  void ChamberOfSecrets(int selectSpiders, Queue < Spider > spiderList)
 {
    for (int index = 0; index < selectSpiders; index++) 
    {
      ArrayList <Spider> spiders = new ArrayList <>();
      int selectedSpiders = spidersToBeSelected(spiderList, selectSpiders);
      for (int counter = 0; counter < selectedSpiders; counter++)
      {
        spiders.add(spiderList.remove());
      }
    
      int maxIndex = findMaxPowerIndex(spiders);
      System.out.print(spiders.get(maxIndex).getIndex()+ " ");
      spiders.remove(maxIndex);
      spiders = decreaseSpidersPower(spiders);
     spiderList.addAll(spiders);
   }
 }

public static void main(String args[])
 {
 new Chamber().getInput();
 }
}