#include<stdio.h>
#include<stdlib.h>
struct Friend
{
	int popularityOfFriend;
	struct Friend *next;
};
struct Friend *topFriend = NULL, *newFriend;

int isValidNumberOfTestCase(testCase)
{
	return 1<=testCase<=1000; 	
}

int isValidNumberOfFriendList(int friendList)
{
	return 1<=friendList<=100000; 	
}

int isValidNumberOfDeleteFriend(int  friendList,int deleteFriend)
{
	return 0<=deleteFriend<friendList;	
}

int isValidPopularityOfFriend(int popularityOfFriend)
{
	return 0<=popularityOfFriend<=100;	
}

struct  Friend* createFriendNode(int  popularityOfFriend )
{
	struct  Friend* newFriend = (struct  Friend*) malloc(sizeof(struct  Friend));
	newFriend->popularityOfFriend = popularityOfFriend;
	newFriend->next = NULL;
	return newFriend;
}

struct Friend* addNewFriend(struct Friend* topFriend, struct Friend* newFriend)
{
	if(topFriend)
	{ 
		newFriend->next = topFriend;
	}
	return newFriend;
}

struct Friend* deleteFriend(struct Friend* topFriend)
{
	if(topFriend == NULL)
	{
		return topFriend;	
	}
	struct Friend *deleteFriendNode = topFriend->next;
	free(topFriend);
	return deleteFriendNode;
}

void printFriendPopularity(struct Friend* head)
{
	if(head == NULL)
		return;
	printFriendPopularity(head->next);
	printf("%d ", head->popularityOfFriend);
}

void getFriendPopularity(int numberOfFriend,int numberOfDeleteFriend)
{

	int friendDeleteCounter,count,popularityOfFriend;
	friendDeleteCounter = numberOfDeleteFriend;
	topFriend = NULL;
	for(count=0; count<numberOfFriend; count++)
	{
		scanf("%d", &popularityOfFriend);
		if(isValidPopularityOfFriend(popularityOfFriend))
		{
			newFriend = createFriendNode(popularityOfFriend);
			while(topFriend!=NULL && topFriend->popularityOfFriend < popularityOfFriend && friendDeleteCounter)
			{
				friendDeleteCounter--;
				topFriend = deleteFriend(topFriend);
			}
			topFriend = addNewFriend(topFriend, newFriend);	
		}
	
	}
	printFriendPopularity(topFriend);
	printf("\n");	
}

int main()
{
	int numberOfFriend,numberOfDeleteFriend,testCase;
	scanf("%d", &testCase);	
	if(isValidNumberOfTestCase){
		while(testCase--)
		{
			scanf("%d %d", &numberOfFriend, &numberOfDeleteFriend);	
			if(isValidNumberOfFriendList(numberOfFriend)&&isValidNumberOfDeleteFriend(numberOfFriend,numberOfDeleteFriend))
			{
				getFriendPopularity( numberOfFriend,numberOfDeleteFriend);
			}
		}
	}
	
	return 0;
}